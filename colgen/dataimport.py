"""Data import
This file is part of the coalgen package.
Copyright 2019 Guilhem Doulcier, Licence GNU GPL3+
"""
import json
import logging
logger = logging.getLogger('coalgen')

def table_to_json(table, name, root=None, dump=True):
    """ Build a json tree from a pandas dataframe """
    if root is None:
        nodes = table.parent.unique()
        if 'ROOT' in nodes:
            root = 'ROOT'
        else:
            for i in nodes:
                if "ROOT" in i:
                    root = i
    if root is None:
        logger.warning('No root in tree {}'.format(name))
        return {}
    if 'time' in table.columns:
        table.sort_values('time', ascending=False, inplace=True)

    tree = {'children':build_tree(table.fillna('nan'),
                                  root,
                                  0),
            'name': name}
    if dump:
        return json.dumps(tree)
    else:
        return tree
    
def build_tree(data, tube, time, ancestor=None):
    '''Recursively find all children node and build a dictionnary tree to be used with d3'''
    out = [row.to_dict()
           for _, row
           in data.loc[data.parent == tube, :].iterrows()]
    for child in out:
        child["children"] = build_tree(data, child['name'], time+1, ancestor)
    return out
