"""interface.py - Command line interface
This file is part of the coalgen package.
Copyright 2019 Guilhem Doulcier, Licence GNU GPL3+
"""

import logging
import os
import webbrowser
import socket
import json
from collections import defaultdict

import pandas as pd

import cherrypy
import cherrypy.process.plugins

from colgen import APP_PATH
import colgen.dataimport


logger = logging.getLogger('coalgen')

class App(object):
    """ Cherrypy app provinding the list of experiments and buttons to start the pipeline """
    def __init__(self, data, theme):
        self.data = data
        self.theme = theme
    @cherrypy.expose
    def index(self):
        with open(os.path.join(APP_PATH, 'web', 'template', 'index.html'),'r') as file:
            html = file.read()
        return html.format(THEME=self.theme)


    @cherrypy.expose
    def tree(self, name=None):
        return self.data[name]['tree']

    @cherrypy.expose
    def mutations(self, name=None):
        if 'mutations' in self.data[name]:
            return self.data[name]['mutations'].to_csv(None, index=False)
        else:
            return ''
    @cherrypy.expose
    def table(self, name=None):
        return self.data[name]['data'].sort_values(by='name').to_csv(None, index=False)

    @cherrypy.expose
    def dataset_list(self):
        return json.dumps({'tree_and_data':[x for x in sorted(self.data.keys())]})

def main(path, port, browser, public, log, theme, mutation_suffix):
    data = {}
    for fi in path:
        bname = os.path.basename(fi).replace('.csv','')
        df = pd.read_csv(fi)
        if 'parent' in df.columns:
            nodes = df.parent.unique()
            root = None
            if 'ROOT' in nodes:
                root = 'ROOT'
            else:
                for i in nodes:
                    if "ROOT" in i:
                        root = i
            if root is not None:
                tree = colgen.dataimport.table_to_json(df,
                                                       name=bname,
                                                       root=root)
                data[bname] = dict(data=df, tree=tree)
            else:
                logger.warning('File {} ignored - no `ROOT` node'.format(bname))
            
        else:
            logger.warning('File {} ignored - no `parent` column'.format(bname))
        mutation_path = fi.replace('.csv',mutation_suffix+'.csv')
        if os.path.exists(mutation_path):
            df_mut = pd.read_csv(mutation_path)
            data[bname]['mutations'] = df_mut
            
    dash = App(data, theme)

    #Mount the app
    conf = {
        '/static':
        {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': os.path.join(APP_PATH, 'web', 'static'),
            'tools.expires.on'    : True,
            'tools.expires.secs'  : 0

        },
    }
    cherrypy.tree.mount(dash, '/', conf)

    # Start the web server.
    start_server(port=int(port),
                 open_webbrowser=browser,
                 public=public,
                 log=log)


def start_server(port=None, open_webbrowser=False, public=False, log=False):
    """ Start the web server.

    Args:
        port: first port to try (will try port up to port+10).
        open_webbrowser (bool): if true try to open the webbrowser on the server.
        public (bool): if true serve on public ip otherwise just on localhost.
        log (bool): Cherrypy logging
    """

    ## Try to find an open port.
    port = int(port)
    iport = port
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as skt:
        while not check_port(port, skt) and port < iport+10:
            port += 1
        if port >= iport+10:
            logger.critical('Failed to find an open port.')
            return False

    ## Configure and start cherrypy
    cherrypy.config.update({
        'server.socket_port': int(port),
        'server.socket_host':'0.0.0.0' if public else '127.0.0.1',
        'log.screen':log,
        'tools.staticdir.debug': log,})

    # Open Web-Browser.
    if open_webbrowser:
        browser = webbrowser.get('firefox')
        cherrypy.engine.start_with_callback(browser.open,
                                            ('http://localhost:{}'.format(port),))
    else:
        cherrypy.engine.start()

    logger.info("Serving {} on http://{}:{}".format('publicly' if public else '',
                                                    '127.0.0.1' if not public else '0.0.0.0',
                                                    port))
    return port

def check_port(port, skt=None):
    """ Check if `port` is in use using the socket `skt`"""
    if skt is None:
        skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        skt.bind(("127.0.0.1", port))
    except OSError as ex:
        # On windows, a OSError "winerror 10048" is thrown if the
        # port is in use.
        logger.info("Port {} is already in use: {}".format(port, ex))
        return False
    except socket.error as ex:
        if ex.errno == 98:
            logger.info("Port {} is already in use".format(port))
            return False
        else:
            raise ex
    else:
        return True
