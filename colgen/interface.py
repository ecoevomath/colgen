"""interface.py - Command line interface
This file is part of the colgen package.
Copyright 2019 Guilhem Doulcier, Licence GNU GPL3+
"""

import sys
import argparse
import logging
import os
import pandas as pd

import colgen.mutation
import colgen.main
import colgen.web

logger = logging.getLogger('coalgen')
logger.setLevel(logging.DEBUG)
levels = [logging.INFO, logging.DEBUG]
console_handler = logging.StreamHandler()
console_handler.setFormatter(logging.Formatter('%(levelname)-7s  %(message)s'))
logger.addHandler(console_handler)

def cli():
    """ Command Line Interface """
    commands = {
        'mutation': (mutation, "Mutation mapping"),
        'survival': (survival, "Survival estimation"),
        'interactive': (interactive, "Interactive visualisation"),
        }
    msg = 'subcommands:\n'
    msg += '\n'.join(["  {}\t {}".format(cmd, help_msg) for cmd, (_, help_msg) in commands.items()])
    msg += '\n\n colgen can also be used as a library in a python script (see readme).'
    parser = argparse.ArgumentParser(description='Collective-level genalogies analysis',
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     epilog=msg)
    parser.add_argument('command', help='Subcommand to use')

    args = parser.parse_args(sys.argv[1:2])
    if args.command in commands:
        commands[args.command][0]()
    else:
        print('Unrecognized command')
        parser.print_help()

def interactive():
    """ CLI for interactive visualisation """
    parser = argparse.ArgumentParser(description='Interactive visualisation')
    parser.add_argument('files', help="Input tree", type=str, nargs="*")
    parser.add_argument('--port', default=8000, type=int, nargs='?',
                        help="Server port")
    parser.add_argument('--public', action='store_true',
                        default=False,
                        help="Serve on public IP")
    parser.add_argument('--browser',
                        default=False,
                        action='store_true',
                        help=("Open browser (dropsignal works best "
                              "with firefox and will try it in priority)"))
    parser.add_argument('--log', action='store_true', help="Webserver logging")
    parser.add_argument('--theme',help="Colour Theme (default: %(default)s)", default='pondscum')
    parser.add_argument('--mutation-suffix',help="Mutations file suffix (default: %(default)s)", default='_mutation_report')

    args = parser.parse_args(sys.argv[2:])
    colgen.web.main(path=args.files, port=args.port, browser=args.browser,
                    public=args.public, log=args.log, theme=args.theme,
                    mutation_suffix=args.mutation_suffix)

def mutation():
    """ CLI for mutation mapping """
    parser = argparse.ArgumentParser(description='Mutation mapping')
    parser.add_argument('--mu', help="Mutation probability (default: %(default)s).", default=0.001, type=float, nargs="?")
    parser.add_argument('--mu_r', help="Mutation reversion probability (default: %(default)s).", default=0.0001, type=float, nargs="?")
    parser.add_argument('--p0', help="Probability of observing a non existing mutation (default: %(default)s).", default=0, type=float, nargs="?")
    parser.add_argument('--p1', help="Probability of observing an existing mutation (default: %(default)s).", default=1, type=float, nargs="?")
    parser.add_argument('files', help="Input tree", type=str, nargs="*")
    parser.add_argument('--out_dir', help="Output folder (default: %(default)s).", type=str, nargs="?", default='.')
    parser.add_argument('--suffix', help="Mutation file suffix (default: %(default)s).", nargs="?", default="_mutations")
    parser.add_argument('--desc_suffix', help="Mutation description file suffix (default: %(default)s).", nargs="?", default="_mutation_description")
    parser.add_argument('-v', '--verbose', help="Verbosity level (default=info, -v=debug)",
                        action='count', default=0)

    args = parser.parse_args(sys.argv[2:])

    level = levels[min(len(levels)-1, args.verbose)]
    console_handler.setLevel(level)

    logger.info('{} files found'.format(len(args.files)))
    for path in args.files:
        logger.debug('{}'.format(path))
        bname = os.path.basename(path).replace('.csv', '')
        node_table = pd.read_csv(path)
        mutation_table = pd.read_csv(path.replace('.csv', args.suffix+'.csv'))
        report, mapping = colgen.mutation.main(node_table, mutation_table,
                                               mu=args.mu,
                                               mu_r=args.mu_r,
                                               p0=args.p0,
                                               p1=args.p1)
        logger.info('=== Mutation mapping === \n {}'.format(mapping))
        mapping.to_csv(os.path.join(args.out_dir, bname+"_mutation_mapping.csv"))
        try:
            mutation_description_table = pd.read_csv(path.replace('.csv', args.desc_suffix+'.csv'))
        except Exception:
            logger.warning('No description file found for {}'.format(bname))
        else:
            report = pd.merge(report.rename(columns={'mutation':'name'}), mutation_description_table, left_on='name', right_on='name', how='left')
        logger.info('=== Report ===\n {}'.format(report))
        report.to_csv(os.path.join(args.out_dir, bname+"_mutation_report.csv"), index=False)

def survival():
    """ CLI for survival estimation """
    parser = argparse.ArgumentParser(description='Survival estimation')
    parser.add_argument('--sigma0', help="Initial sigma", default=0.5, type=float, nargs="?")
    parser.add_argument('--sigma_max', help="Maximal value of sigma", default=1, type=float, nargs="?")
    parser.add_argument('--sigma_min', help="Minimal value of sigma", default=0.1, type=float, nargs="?")
    parser.add_argument('--max_em_steps', help="Max number of steps", default=20, type=float, nargs="?")
    parser.add_argument('--steps', help="Grid size", default=10, type=int, nargs="?")
    parser.add_argument('--outpath', help="Output path", default='output', type=str, nargs="?")
    parser.add_argument('--model', help="Heredity Model", default='truncnorm', type=str, nargs="?")
    parser.add_argument('files', help="Input tree", type=str, nargs="*")
    parser.add_argument('-v', '--verbose', help="Verbosity level (default=info, -v=debug)",
                        action='count', default=0)

    args = parser.parse_args(sys.argv[2:])

    level = levels[min(len(levels)-1, args.verbose)]
    console_handler.setLevel(level)



    if args.model=='truncnorm':
        import colgen.models.truncnorm
        model = colgen.models.truncnorm
    elif args.model=="jump":
        import colgen.models.jump
        model = colgen.models.jump

    sigma_range = (args.sigma0, args.sigma_min, args.sigma_max)

    for path in args.files:
        try:
            df, convergence = colgen.main.fit(csv=path,
                                       sigma_range=sigma_range,
                                       steps=args.steps,
                                       max_em_steps=args.max_em_steps,
                                       model=model)
            logger.info('Convergence info:\n{}'.format(convergence))
            bname = os.path.basename(path)
            if not os.path.exists(args.outpath):
                os.mkdir(args.outpath)
            outfile = os.path.join(args.outpath, "grid{}_model{}_{}".format(
                args.steps, args.model, bname)).replace('.json', '.csv')
            logger.info('Export to {}'.format(outfile))

            sigma = convergence.sigma.values[-1]
            likelihood = convergence.likelihood.values[-1]
            print('{} sigma:{}, liklihood: {}:\n{}'.format(path, sigma, likelihood, df))
            df.to_csv(outfile)
            convergence.to_csv(outfile.replace('.csv', '.convergence'))

        except Exception as ex:
            print('ERROR:', ex)
            raise ex
