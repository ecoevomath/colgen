""" Survival propagation model 
This file is part of the coalgen package.
Copyright 2020 Guilhem Doulcier, Licence GNU GPLv3+
"""
from functools import partial, lru_cache
import warnings
import logging

import numpy as np
import matplotlib.pyplot as plt
import colgen.models.beta as beta
import colgen.models.truncnorm as truncnorm
import colgen.models.jump as jump

logger = logging.getLogger('coalgen')
DEBUG = True


def generate_log_potential_and_domain(model_module, grid):
    @lru_cache(maxsize=None)
    def log_potential(t, parent, is_extinct, sigma, grid):
        val = model_module.model(t, parent=parent,
                                 is_extinct=is_extinct,
                                 sigma=sigma, grid=grid)
        with warnings.catch_warnings():
            warnings.filterwarnings('error')
            try:
                return np.log(val)
            except Warning:
                if val == 0:
                    return -1e300
                else:
                    raise ValueError('Negative Probability {}'.format(val))
                # logger.warning(("Underflow value in potential: "
                #                 "trait={}, parent={}, is_extinct={}, "
                #                 "sigma={}, grid={}, value={}. Set to 1e-300."
                #                 "").format(t, parent, is_extinct, sigma, grid, val))
    return log_potential, model_module.get_domain(grid)


def plot_model(model_module, sigma, grid):
    fig, ax = plt.subplots(2, 2, figsize=(8, 8))
    domain = model_module.get_domain(grid)
    f = np.vectorize(partial(model_module.model,
                             sigma=sigma, grid=len(domain)))
    X, Y = np.meshgrid(domain, domain)
    Z = f(X, Y, 0)
    ZZ = f(X, Y, 1)
    ax[0, 0].imshow(Z)
    ax[0, 1].imshow(ZZ)

    parent = domain[2*len(domain)//3]
    for i, parent in enumerate([domain[len(domain)//3], domain[2*len(domain)//3]]):
        ax[1, i].plot(domain, f(domain, parent, 1), label='Extinct')
        ax[1, i].scatter(domain, f(domain, parent, 1))
        ax[1, i].plot(domain, f(domain, parent, 0), label='Survived')
        ax[1, i].scatter(domain, f(domain, parent, 0))
        ax[1, i].scatter(domain, f(domain, parent, 1) +
                         f(domain, parent, 0), color='k')
        ax[1, i].plot(domain, f(domain, parent, 1) +
                      f(domain, parent, 0), color='k', label='No observation')
        ax[1, i].vlines(parent, 0, f(parent, parent, 1)+f(parent, parent, 0),
                        color='C2', linestyle='--', label='parent')
        ax[1, i].legend()
    ax[0, 0].set(title='P(X=t|X_p_i=p)P(E=0|X=t))')
    ax[0, 1].set(title='P(X=t|X_p_i=p)P(E=1|X=t))')
    return fig, ax
