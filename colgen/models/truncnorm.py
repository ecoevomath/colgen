""" Truncated normal model 
This file is part of the coalgen package.
Copyright 2020 Guilhem Doulcier, Licence GNU GPLv3+
"""
from functools import partial
import scipy.stats
import scipy.integrate
import numpy as np
import pytest


def model(t, parent, is_extinct, sigma, grid):
    """Return P(X=t|X_p_i=parent)P(E=is_extinct|X=t)

    t: trait of the focal node
    parent: trait of the parental node
    is_extinct: true if the node is extinct
    sigma: confidence
    grid: domain size
    """
    if not sigma:
        return 1/(2*grid**2)
    dx = 1/(grid*2)
    rv = scipy.stats.truncnorm(a=(0 - parent) / sigma,
                               b=(1 - parent) / sigma,
                               loc=parent, scale=sigma)
    v = (lambda t: 1-t) if is_extinct else (lambda t: t)
    a = t-dx
    b = t+dx
    assert 0 <= a <= 1
    assert 0 <= b <= 1
    proba = rv.cdf(b)*v(b) - rv.cdf(a)*v(a)
    proba += (1 if is_extinct else -1) * scipy.integrate.quad(rv.cdf, a, b)[0]
    proba /= grid

    # if proba == 0:
    #     print('-'*80)
    #     print('trait={}; parent={}; is_extinct={}; sigma={}'.format(
    #         t, parent, is_extinct, sigma))
    #     print('a={},b={}, v(a)={},v(b)={}'.format(a, b, v(a), v(b)))
    #     print('[cx]', rv.cdf(b)*v(b) - rv.cdf(a)*v(a), '=',
    #           rv.cdf(b), '*', v(b), '-', rv.cdf(a), '*',  v(a))
    #     print(rv.cdf(b) == rv.cdf(a))
    #     print(rv.logcdf(b) == rv.logcdf(a))
    #     print(rv.sf(b) == rv.sf(a))
    #     print('+/- int c', (1 if is_extinct else -1)
    #           * scipy.integrate.quad(rv.cdf, a, b)[0])
    #     print('=', proba)
    #     print('norm:', proba)

    if proba < 0:
        np.testing.assert_almost_equal(proba, 0)
        return 0
    return proba


def get_domain(steps):
    dx = 1/(steps*2)
    return np.linspace(0+dx, 1-dx, steps)


