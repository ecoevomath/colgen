import pandas as pd
import colgen.main
import colgen.models.jump as jp


def test_fit():
    mock_data = pd.DataFrame([{'name':'C1','parent':'ROOT',  'extinct':0},
                              {'name':'C2','parent':'C1',  'extinct':1},
                              {'name':'C3','parent':'C1',  'extinct':1},
                              {'name':'C4','parent':'C3',  'extinct':1}])
    df, conv = colgen.main.fit(mock_data, jp, (0.5,0,1))
    print(df)
    print(conv)
    assert 'survival_estimate' in df
