import numpy as np
import pytest 
from colgen.models.jump import get_domain, model
from functools import partial



@pytest.fixture(params=[8, 4], scope='module')
def domain(request):
    return get_domain(request.param)


@pytest.fixture(params=[0, 0.5, 1], scope='module')
def distrib(domain, request):
    X, Y, Z = np.meshgrid(domain, domain, [0, 1])
    g = np.vectorize(partial(model, sigma=request.param, grid=len(domain)))
    out = g(X, Y, Z)
    return out


def test_probability(distrib):
    assert distrib.max() <= 1, 'Probabilities should be less than 1'
    assert distrib.min() >= 0, 'Probabilities should be positive'


def test_total_probability(distrib):
    np.testing.assert_almost_equal(distrib.sum(), 1,
                                   err_msg=r'\sum_p\sum_t\sum_e P(X=t|Xp=p)P(E=e|X=t) = \sum_t\sum_e P(X=t,E=e)  = 1')


def test_marginal_probability_of_e(distrib):
    prob_of_e = distrib.sum(0).sum(0)
    np.testing.assert_almost_equal(prob_of_e, (0.5, 0.5),
                                   err_msg=r'\sum_p\sum_t P(X=t|Xp=p)P(E=e|X=t) = \sum_t P(X=t,E=e) = P(E=e)  = (0.5,0.5)')


def test_marginal_probability_of_x(distrib):
    prob_of_x = distrib.sum(1).sum(1)
    np.testing.assert_almost_equal(prob_of_x, len(prob_of_x)*[1/len(prob_of_x)],
                                   err_msg=r'\sum_p\sum_t P(X=t|Xp=p)P(E=e|X=t) = \sum_e P(X=t,E=e) = P(X=t)  = 1/Card(t)')


def test_sigma_equal_one(domain):
    """
    If sigma == 1, the survival probability of the child is the same as its parent.
    I.e. P(E=0) = parent"""
    X, Y, Z = np.meshgrid(domain, domain, [0, 1])
    g = np.vectorize(partial(model, sigma=1, grid=len(domain)))
    out = g(X, Y, Z)
    np.testing.assert_almost_equal(out.sum(1)[:, 0]*len(domain), domain)


def test_sigma_equal_zero(domain):
    """
    If sigma == 0, the survival probability should be always the same regardless of the parent
    I.e. P(E=0) = 0.5
    """
    X, Y, Z = np.meshgrid(domain, domain, [0, 1])
    g = np.vectorize(partial(model, sigma=0, grid=len(domain)))
    out = g(X, Y, Z)
    np.testing.assert_almost_equal(out.sum(1)[:, 0]*len(domain), 0.5)
