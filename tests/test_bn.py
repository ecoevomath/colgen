""" Tests """
import numpy as np
from colgen.bn_discrete import Potential, factor_sum, max_marginalise, log_likelihood, expectation_maximisation
import pytest
import scipy.integrate
import scipy.stats
import colgen.models
from functools import partial, lru_cache


@pytest.fixture()
def simple_potentials():
    """ A in {0, 1}
        B in {0, 0.5, 1.0}
    P(A=x) = x
    P(A=x|B=y) = y*(1-y)"""
    dv = {0: [0.0, 1.0], 1: [0.0, 0.5, 1.0]}
    F1 = Potential([0], lambda x: x, domain_var=dv)
    F2 = Potential([0, 1], lambda x, y: y*(1-y), domain_var=dv)
    return F1, F2


@pytest.fixture()
def potential_sum(simple_potentials):
    F1, F2 = simple_potentials
    return factor_sum([F1, F2])


def test_potential(simple_potentials):
    """ Potentials are correctly initiated """
    F1, F2 = simple_potentials
    np.testing.assert_equal(F1.value, np.array([0.0, 1.0]))
    np.testing.assert_equal(F2.value, np.array([[0.0, 0.25, 0.0],
                                                [0.0, 0.25, 0.0]]))


def test_sum(potential_sum):
    """ Potential are correctly summed """
    np.testing.assert_equal(potential_sum.value,
                            np.array([[0.0, 0.25, 0.0],
                                      [1.0, 1.25, 1.0]]))


def test_max_marginalise_sum(potential_sum):
    """ Max marginalisation of F1+F2"""
    M = max_marginalise(potential_sum, 0)
    np.testing.assert_equal(M.value, np.array([1.0, 1.25, 1.0]))
    np.testing.assert_equal(M.argmax, np.array([1, 1, 1]))
    np.testing.assert_equal(M.argmax_pos, np.array([1, 1, 1]))

    M2 = max_marginalise(potential_sum, 1)
    np.testing.assert_equal(M2.value, np.array([0.25, 1.25]))
    np.testing.assert_equal(M2.argmax, np.array([0.5, 0.5]))
    np.testing.assert_equal(M2.argmax_pos, np.array([1, 1]))


def test_max_marginalise_double(simple_potentials):
    """ Max marginalisae F2 twice """
    _, F2 = simple_potentials
    M3 = max_marginalise(max_marginalise(F2, 0), 1)
    np.testing.assert_equal(M3.value, 0.25)


@pytest.fixture(params=[(0, 0), (1, 1), (0, 1), (1, 0)], scope='module')
def simple_net(request):
    return dict(
        root=0.5,
        ta=0.8,
        tb=0.5,
        extinct_a=request.param[0],
        extinct_b=request.param[0],
        grid=101)


@pytest.fixture
def factors_jump_2var(simple_net):
    pot, domain_var = colgen.models.generate_log_potential_and_domain(colgen.models.jump,
                                                                      simple_net['grid'])

    def _factor_generator(sigma_value, domain_var):
        factors = [
            Potential([0], partial(pot,
                                   parent=simple_net['root'],
                                   is_extinct=simple_net['extinct_a'],
                                   sigma=sigma_value,
                                   grid=simple_net['grid']),
                      domain_var=domain_var),
            Potential([1, 0],
                      partial(pot,
                              is_extinct=simple_net['extinct_b'],
                              sigma=sigma_value,
                              grid=simple_net['grid']),
                      domain_var=domain_var),
        ]
        return factors
    return pot, domain_var, _factor_generator


@pytest.fixture
def factors_jump_1var(simple_net):
    pot, domain_var = colgen.models.generate_log_potential_and_domain(colgen.models.jump,
                                                                      simple_net['grid'])

    def _factor_generator(sigma_value, domain_var):
        factors = [
            Potential([0], partial(pot,
                                   parent=simple_net['root'],
                                   is_extinct=simple_net['extinct_a'],
                                   sigma=sigma_value,
                                   grid=simple_net['grid']),
                      domain_var=domain_var)]
        return factors
    return pot, domain_var, _factor_generator


@pytest.mark.parametrize("sigma0", [1e-9, 0.5, 1-1e-9])
def test_em1var(simple_net, factors_jump_1var, sigma0):
    pot, domain_var, factor_generator = factors_jump_1var
    sigma_val, likelihoods, map_config = expectation_maximisation(factor_generator, {0: domain_var},
                                                                  [0], sigma0, theta_min=1e-9,
                                                                  theta_max=1-1e-9, mxsteps=10,
                                                                  tol=1e-13)
#    raise ValueError("E={} OUT:{}, S={}, LK={} | S={}, L={}".format(simple_net['extinct_a'], map_config,
#                                                                    sigma_val[-1], likelihoods[-1], sigma_val, likelihoods))


@pytest.mark.parametrize("sigma", [1e-9, 0.5, 0.9, 1-1e-9])
def test_log_likelihood_sum(simple_net, factors_jump_2var, sigma):
    pot, domain_var, factor_generator = factors_jump_2var
    lpa = pot(simple_net['ta'], simple_net['root'],
              is_extinct=simple_net['extinct_a'],
              sigma=sigma, grid=simple_net['grid'])
    lpb = pot(simple_net['tb'], simple_net['ta'],
              is_extinct=simple_net['extinct_b'],
              sigma=sigma, grid=simple_net['grid'])
    a = log_likelihood(sigma,
                       config={0: simple_net['ta'], 1: simple_net['tb']},
                       factor_generator=factor_generator,
                       ordering=[1, 0])
    np.testing.assert_approx_equal(a, lpa+lpb)


@pytest.mark.parametrize("sigma0", [1e-9, 0.5, 0.9, 1-1e-9])
def test_em(simple_net, factors_jump_2var, sigma0):
    pot, domain_var, factor_generator = factors_jump_2var
    sigma_val, likelihoods, map_config = expectation_maximisation(factor_generator, {0: domain_var, 1: domain_var},
                                                                  [0, 1], sigma0, theta_min=0,
                                                                  theta_max=1, mxsteps=10,
                                                                  tol=1e-13)
    # Initial value of sigma should be the same.
    np.testing.assert_almost_equal(sigma_val[0], sigma0)

    # Likelihoods should be increasing
    np.testing.assert_equal(likelihoods[1:], sorted(likelihoods[1:]))

    # In this simple network the MAP is 0 if extinct, 1 if survived.
    assert map_config[0] == 0.5
    assert map_config[1] == 0.5

    # In this simple network the MAP is 0 if extinct, 1 if survived.
    # assert map_config[0] == int(not simple_net['extinct_a'])
    # assert map_config[1] == int(not simple_net['extinct_b'])
    #raise ValueError(map_config, sigma_val, likelihoods, domain_var)
# assert sigma_val[-1] == (1 if (simple_net['extinct_a'] ==
#                                   simple_net['extinct_b']) else 0)
